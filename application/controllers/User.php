<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {
	public function __construct() {

       parent::__construct();

       $this->load->database();

    }

	public function index()
	{
		$this->load->view('user_details');
	}
	public function saveUserDetails()
	{
		$result['name']=$this->input->post('user_name');
		$result['email']=$this->input->post('email');
		$result['phone']=$this->input->post('phone');
		$result['gender']=$this->input->post('gender');
		$result['password']=$this->input->post('password');
	    foreach ($this->input->post('addmore') as $value) {
	        $result['qualification']=$value['qualification'];
	        $result['passed_year']=$value['passed_year'];
	        $result['percentage']=$value['percentage'];
	    }

	    // return $this->db->insert('user_details',$result);
	}
}
