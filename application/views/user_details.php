<!DOCTYPE html>

<html>

<head>

    <title>FININFOCOM</title>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" />  

    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>

    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>

</head>

<body>

   

<div class="container">

    <h2 align="center">User Details</h2>  

    <div class="form-group">

         <form id="user_form" method="POST" action="" onsubmit="return false;">
          <label>Full Name:</label><input type="text" name="user_name"><br>
           <label>Email:</label><input type="email" name="email"><br>
           <label>Mobile No :</label><input type="phone" name="phone"><br>
           <label>Gender :</label><input type="radio" id="male" name="gender" value="male">
            <label for="male">Male</label>
            <input type="radio" id="female" name="gender" value="female">
            <label for="female">Female</label><br>
           <label>Password:</label><input type="password" name="password"><br>
           <label>Confirm Password:</label><input type="password" name="confirm_password">
  

            <div class="table-responsive">  
              <td><button type="button" name="add" id="add" class="btn btn-success">Add More</button></td>

                <table class="table table-bordered" id="dynamic_field"> 
                <thead>
                  <tr>
                    <td>S.No</td>
                    <td>Qualification</td>
                    <td>Passed year</td>
                    <td>Percentage(%)</td>
                    <td>Remove</td>
                  </tr>
                </thead> 
                <tbody>
                  <tr> 
                      <td>1.</td> 
                        <td><input type="text" name="addmore[][qualification]" placeholder="Qualification" class="form-control name_list" required="" /></td>
                        <td><input type="text" name="addmore[][passed_year]" placeholder="Year of Passed Out" class="form-control name_list" required="" /></td>
                        <td><input type="text" name="addmore[][percentage]" placeholder="Percentage" class="form-control name_list" required="" /></td>
                        <td><button type="button" name="remove" id="1" class="btn btn-danger btn_remove">X</button></td>  
                    </tr> 
                </tbody>
                </table>  

                <input type="submit" name="submit" id="submit" class="btn btn-info" value="Submit" />  

            </div>

   

         </form>  

    </div> 

</div>

   

<script type="text/javascript">
    $(document).ready(function(){      
      var i=1;  
      $('#add').click(function(){  
           i++;  
           $('#dynamic_field').append('<tr id="row'+i+'" class="dynamic-added"><td>'+i+'</td><td><input type="text" name="addmore[][qualification]" placeholder="Qualification" class="form-control name_list" required /></td><td><input type="text" name="addmore[][passed_year]" placeholder="Year of Passed Out" class="form-control name_list" required="" /></td><td><input type="text" name="addmore[][percentage]" placeholder="Percentage" class="form-control name_list" required="" /></td><td><button type="button" name="remove" id="'+i+'" class="btn btn-danger btn_remove">X</button></td></tr>');  
      });

      $(document).on('click', '.btn_remove', function(){  
        i--;
           var button_id = $(this).attr("id");   
           $('#row'+button_id+'').remove();  
      });  
    });  


$("#user_form").on('submit',function () {
  var form_data=new FormData();
  form_data.append("name","Aparna");
  $.ajax({
    type:"POST",
    url:"http://fininfocom/saveUserDetails",
    data:form_data,
    processData:false,
    contentType:false,
    success:function (data) {
      console.log(data);
    },
    error:function (error) {
      console.log(error);
    }
  })
})
</script>

</body>

</html>